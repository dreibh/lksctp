/* SCTP kernel implementation
 * (C) Copyright IBM Corp. 2001, 2004
 * Copyright (c) 1999-2000 Cisco, Inc.
 * Copyright (c) 1999-2001 Motorola, Inc.
 * Copyright (c) 2002 Intel Corp.
 *
 * This file is part of the SCTP kernel implementation
 *
 * This header represents the structures and constants needed to support
 * the SCTP Extension to the Sockets API.
 *
 * This SCTP implementation is free software;
 * you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This SCTP implementation is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *                 ************************
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNU CC; see the file COPYING.  If not, write to
 * the Free Software Foundation, 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Please send any bug reports or fixes you make to the
 * email address(es):
 *    lksctp developers <linux-sctp@vger.kernel.org>
 *
 * Or submit a bug report through the following website:
 *    http://www.sf.net/projects/lksctp
 *
 * Written or modified by:
 *    La Monte H.P. Yarroll    <piggy@acm.org>
 *    R. Stewart               <randall@sctp.chicago.il.us>
 *    K. Morneau               <kmorneau@cisco.com>
 *    Q. Xie                   <qxie1@email.mot.com>
 *    Karl Knutson             <karl@athena.chicago.il.us>
 *    Jon Grimm                <jgrimm@us.ibm.com>
 *    Daisy Chang              <daisyc@us.ibm.com>
 *    Ryan Layer               <rmlayer@us.ibm.com>
 *    Ardelle Fan              <ardelle.fan@intel.com>
 *    Sridhar Samudrala        <sri@us.ibm.com>
 *    Inaky Perez-Gonzalez     <inaky.gonzalez@intel.com>
 *    Vlad Yasevich            <vladislav.yasevich@hp.com>
 *
 * Any bugs reported given to us we will try to fix... any fixes shared will
 * be incorporated into the next SCTP release.
 */

#ifndef __linux_sctp_h__
#define __linux_sctp_h__

#include <stdint.h>
#include <linux/types.h>
#include <linux/socket.h>
__BEGIN_DECLS

typedef __s32 sctp_assoc_t;

/* Socket option layer for SCTP */
#ifndef SOL_SCTP
#define SOL_SCTP	132
#endif

#ifndef IPPROTO_SCTP
#define IPPROTO_SCTP    132
#endif

/* 9. Preprocessor constants */
#define HAVE_SCTP
#define HAVE_KERNEL_SCTP
#define HAVE_SCTP_MULTIBUF
#define HAVE_SCTP_NOCONNECT
#define HAVE_SCTP_PRSCTP
#define HAVE_SCTP_ADDIP
#define HAVE_SCTP_CANSET_PRIMARY


/* The following symbols come from the Sockets API Extensions for
 * SCTP <draft-ietf-tsvwg-sctpsocket-07.txt>.
 */
#define SCTP_RTOINFO	0
#define SCTP_ASSOCINFO  1
#define SCTP_INITMSG	2
#define SCTP_NODELAY	3		/* Get/set nodelay option. */
#define SCTP_AUTOCLOSE	4
#define SCTP_SET_PEER_PRIMARY_ADDR 5	/* Write only */
#define SCTP_PRIMARY_ADDR	6
#define SCTP_ADAPTATION_LAYER	7
#define SCTP_DISABLE_FRAGMENTS	8
#define SCTP_PEER_ADDR_PARAMS	9
#define SCTP_DEFAULT_SEND_PARAM	10
#define SCTP_EVENTS	11
#define SCTP_I_WANT_MAPPED_V4_ADDR 12	/* Turn on/off mapped v4 addresses  */
#define SCTP_MAXSEG	13		/* Get/set maximum fragment. */
#define SCTP_STATUS	14		/* Read only */
#define SCTP_GET_PEER_ADDR_INFO	15	/* Read only */
#define SCTP_DELAYED_ACK_TIME	16
#define SCTP_DELAYED_ACK SCTP_DELAYED_ACK_TIME
#define SCTP_DELAYED_SACK SCTP_DELAYED_ACK_TIME
#define SCTP_CONTEXT	17
#define SCTP_FRAGMENT_INTERLEAVE	18
#define SCTP_PARTIAL_DELIVERY_POINT	19 /* Set/Get partial delivery point */
#define SCTP_MAX_BURST	20		/* Set/Get max burst */
#define SCTP_AUTH_CHUNK	21	/* Set only: add a chunk type to authenticate */
#define SCTP_HMAC_IDENT	22
#define SCTP_AUTH_KEY	23
#define SCTP_AUTH_ACTIVE_KEY	24
#define SCTP_AUTH_DELETE_KEY	25
#define SCTP_PEER_AUTH_CHUNKS	26	/* Read only */
#define SCTP_LOCAL_AUTH_CHUNKS	27	/* Read only */
#define SCTP_GET_ASSOC_NUMBER	28	/* Read only */
#define SCTP_GET_ASSOC_ID_LIST	29	/* Read only */
#define SCTP_AUTO_ASCONF       30
#define SCTP_PEER_ADDR_THLDS	31

#define SCTP_DEFAULT_PRINFO	32
#define SCTP_EXPLICIT_EOR	33
#define SCTP_REUSE_PORT		34
#define SCTP_EVENT 35
#define SCTP_RECVRCVINFO	36
#define SCTP_RECVNXTINFO	37
#define SCTP_DEFAULT_SNDINFO	38
#define SCTP_AUTH_DEACTIVATE_KEY 39


/* Internal Socket Options. Some of the sctp library functions are
 * implemented using these socket options.
 */
#define SCTP_SOCKOPT_BINDX_ADD	100	/* BINDX requests for adding addrs */
#define SCTP_SOCKOPT_BINDX_REM	101	/* BINDX requests for removing addrs. */
#define SCTP_SOCKOPT_PEELOFF	102	/* peel off association. */
/* Options 104-106 are deprecated and removed. Do not use this space */
#define SCTP_SOCKOPT_CONNECTX_OLD	107	/* CONNECTX old requests. */
#define SCTP_GET_PEER_ADDRS	108		/* Get all peer address. */
#define SCTP_GET_LOCAL_ADDRS	109		/* Get all local address. */
#define SCTP_SOCKOPT_CONNECTX	110		/* CONNECTX requests. */
#define SCTP_SOCKOPT_CONNECTX3	111	/* CONNECTX requests (updated) */
#define SCTP_GET_ASSOC_STATS	112	/* Read only */

/*
 * 5.3.1 SCTP Initiation Structure (SCTP_INIT)
 *
 *   This cmsghdr structure provides information for initializing new
 *   SCTP associations with sendmsg().  The SCTP_INITMSG socket option
 *   uses this same data structure.  This structure is not used for
 *   recvmsg().
 *
 *   cmsg_level    cmsg_type      cmsg_data[]
 *   ------------  ------------   ----------------------
 *   IPPROTO_SCTP  SCTP_INIT      struct sctp_initmsg
 *
 */
struct sctp_initmsg {
	__u16 sinit_num_ostreams;
	__u16 sinit_max_instreams;
	__u16 sinit_max_attempts;
	__u16 sinit_max_init_timeo;
};

/*
 * 5.2.2 SCTP Header Information Structure (SCTP_SNDRCV) 
 *
 *   This cmsghdr structure specifies SCTP options for sendmsg() and
 *   describes SCTP header information about a received message through
 *   recvmsg().
 *
 *   (DEPRECATED) - SCTP_SNDINFO (described in Section 5.3.4) and SCTP_RCVINFO
 *                  (described in Section 5.3.5) split this information.  These 
 * 	            structures should be used, when possible, since SCTP_SNDRCV 
 *                  is deprecated.
 *
 *   cmsg_level    cmsg_type      cmsg_data[]
 *   ------------  -----------    ----------------------
 *   IPPROTO_SCTP  SCTP_SNDRCV    struct sctp_sndrcvinfo
 *
 */
struct sctp_sndrcvinfo {
	__u16 sinfo_stream;
	__u16 sinfo_ssn;
	__u16 sinfo_flags;
	__u32 sinfo_ppid;
	__u32 sinfo_context;
	__u32 sinfo_timetolive;
	__u32 sinfo_tsn;
	__u32 sinfo_cumtsn;
	sctp_assoc_t sinfo_assoc_id;
};

/* 
 * 5.3.4 SCTP Send Information Structure (SCTP_SNDINFO)
 *
 *   This cmsghdr structure specifies SCTP options for sendmsg().
 *
 *   cmsg_level    cmsg_type      cmsg_data[]
 *   ------------  ------------   -------------------
 *   IPPROTO_SCTP  SCTP_SNDINFO   struct sctp_sndinfo
 *
 */

struct sctp_sndinfo {
	__u16 snd_sid;
	__u16 snd_flags;
	__u32 snd_ppid;
	__u32 snd_context;
	sctp_assoc_t snd_assoc_id;
};

/*
 * 5.3.5 SCTP Receive Information Structure (SCTP_RCVINFO)
 *
 *   This cmsghdr structure specifies SCTP options for sendmsg().
 *
 *   cmsg_level    cmsg_type      cmsg_data[]
 *   ------------  ------------   -------------------
 *   IPPROTO_SCTP  SCTP_RCVINFO   struct sctp_rcvinfo
 *
 */
struct sctp_rcvinfo {
	__u16 rcv_sid;
	__u16 rcv_ssn;
	__u16 rcv_flags;
	__u32 rcv_ppid;
	__u32 rcv_tsn;
	__u32 rcv_cumtsn;
	__u32 rcv_context;
	sctp_assoc_t rcv_assoc_id;
};

/*
 * 5.3.6. SCTP Next Receive Information Structure (SCTP_NXTINFO)
 *
 *   This cmsghdr structure describes SCTP receive information of the next
 *   message that will be delivered through recvmsg() if this information
 *   is already available when delivering the current message.
 *
 *   cmsg_level    cmsg_type      cmsg_data[]
 *   ------------  ------------   -------------------
 *   IPPROTO_SCTP  SCTP_NXTINFO   struct sctp_nxtinfo
 *
 */
struct sctp_nxtinfo {
    __u16 nxt_sid;
    __u16 nxt_flags;
    __u32 nxt_ppid;
    __u32 nxt_length;
    sctp_assoc_t nxt_assoc_id;
}; 

/*
 * 5.3.7. SCTP PR-SCTP Information Structure (SCTP_PRINFO)
 *
 * This cmsghdr structure specifies SCTP options for sendmsg().
 *
 *   cmsg_level    cmsg_type      cmsg_data[]
 *   ------------  -----------   ------------------
 *   IPPROTO_SCTP  SCTP_PRINFO   struct sctp_prinfo
 *
 */

struct sctp_prinfo {
    __u16 pr_policy;
    __u32 pr_value;
};

/*
 * 5.3.8 SCTP AUTH Information Structure (SCTP_AUTHINFO)
 *
 * This cmsghdr structure specifies SCTP options for sendmsg().
 *
 *   cmsg_level    cmsg_type      cmsg_data[]
 *   ------------  ------------   ------------------
 *   IPPROTO_SCTP  SCTP_AUTHNFO   struct sctp_authinfo
 *
 */
struct sctp_authinfo {
    __u16 auth_keynumber;
};

/*
 *  sinfo_flags: 16 bits (unsigned integer)
 *
 *   This field may contain any of the following flags and is composed of
 *   a bitwise OR of these values.
 */

enum sctp_sinfo_flags {
	SCTP_UNORDERED = 1,  /* Send/receive message unordered. */
	SCTP_ADDR_OVER = 2,  /* Override the primary destination. */
	SCTP_ABORT=4,        /* Send an ABORT message to the peer. */
	SCTP_SACK_IMMEDIATELY = 8,	/* SACK should be sent without delay */
	SCTP_EOF=MSG_FIN,    /* Initiate graceful shutdown process. */
	SCTP_SENDALL = 16,
	SCTP_COMPLETE = 32,
	SCTP_NOTIFICATION = 64,
};

typedef union {
	__u8			raw;
	struct sctp_initmsg	init;
	struct sctp_sndrcvinfo	sndrcv;
} sctp_cmsg_data_t;

/* These are cmsg_types.  */
typedef enum sctp_cmsg_type {
	SCTP_INIT,              /* 5.3.1 SCTP Initiation Structure */
#define SCTP_INIT	SCTP_INIT
	SCTP_SNDRCV,            /* 5.3.2 SCTP Header Information Structure */
#define SCTP_SNDRCV	SCTP_SNDRCV
	SCTP_SNDINFO,
#define SCTP_SNDINFO	SCTP_SNDINFO
	SCTP_RCVINFO,
#define SCTP_RCVINFO	SCTP_RCVINFO
	SCTP_NXTINFO,
#define SCTP_NXTINFO	SCTP_NXTINFO
	SCTP_PRINFO,
#define SCTP_PRINFO	SCTP_PRINFO
	SCTP_AUTHINFO,
#define SCTP_AUTHINFO	SCTP_AUTHINFO
	SCTP_DSTADDRV4,
#define SCTP_DSTADDRV4	SCTP_DSTADDRV4
	SCTP_DSTADDRV6,
#define SCTP_DSTADDRV6	SCTP_DSTADDRV6
} sctp_cmsg_t;

/*
 * 6.1.1. SCTP_ASSOC_CHANGE
 *
 *   Communication notifications inform the ULP that an SCTP association
 *   has either begun or ended. The identifier for a new association is
 *   provided by this notificaion. The notification information has the
 *   following format:
 *
 */
struct sctp_assoc_change {
	__u16 sac_type;
	__u16 sac_flags;
	__u32 sac_length;
	__u16 sac_state;
	__u16 sac_error;
	__u16 sac_outbound_streams;
	__u16 sac_inbound_streams;
	sctp_assoc_t sac_assoc_id;
	__u8 sac_info[0];
};

/* These are the available sac_info features. */
typedef enum sctp_sac_info_features {
    SCTP_ASSOC_SUPPORTS_PR,
#define SCTP_ASSOC_SUPPORTS_PR 		SCTP_ASSOC_SUPPORTS_PR
    SCTP_ASSOC_SUPPORTS_AUTH,
#define SCTP_ASSOC_SUPPORTS_AUTH 	SCTP_ASSOC_SUPPORTS_AUTH
    SCTP_ASSOC_SUPPORTS_ASCONF,
#define SCTP_ASSOC_SUPPORTS_ASCONF 	SCTP_ASSOC_SUPPORTS_ASCONF
    SCTP_ASSOC_SUPPORTS_MULTIBUF,
#define SCTP_ASSOC_SUPPORTS_MULTIBUF 	SCTP_ASSOC_SUPPORTS_MULTIBUF
} sctp_sac_info_t;

/*
 *   sac_state: 32 bits (signed integer)
 *
 *   This field holds one of a number of values that communicate the
 *   event that happened to the association.  They include:
 *
 *   Note:  The following state names deviate from the API draft as
 *   the names clash too easily with other kernel symbols.
 */
enum sctp_sac_state {
	SCTP_COMM_UP,
	SCTP_COMM_LOST,
	SCTP_RESTART,
	SCTP_SHUTDOWN_COMP,
	SCTP_CANT_STR_ASSOC,
};

/*
 * 6.1.2. SCTP_PEER_ADDR_CHANGE
 *
 *   When a destination address on a multi-homed peer encounters a change
 *   an interface details event is sent.  The information has the
 *   following structure:
 */
struct sctp_paddr_change {
	__u16 spc_type;
	__u16 spc_flags;
	__u32 spc_length;
	struct sockaddr_storage spc_aaddr;
	int spc_state;
	int spc_error;
	sctp_assoc_t spc_assoc_id;
} __attribute__((packed, aligned(4)));

/*
 *    spc_state:  32 bits (signed integer)
 *
 *   This field holds one of a number of values that communicate the
 *   event that happened to the address.  They include:
 */
enum sctp_spc_state {
	SCTP_ADDR_AVAILABLE,
	SCTP_ADDR_UNREACHABLE,
	SCTP_ADDR_REMOVED,
	SCTP_ADDR_ADDED,
	SCTP_ADDR_MADE_PRIM,
	SCTP_ADDR_CONFIRMED,
};


/*
 * 6.1.3. SCTP_REMOTE_ERROR
 *
 *   A remote peer may send an Operational Error message to its peer.
 *   This message indicates a variety of error conditions on an
 *   association. The entire error TLV as it appears on the wire is
 *   included in a SCTP_REMOTE_ERROR event.  Please refer to the SCTP
 *   specification [SCTP] and any extensions for a list of possible
 *   error formats. SCTP error TLVs have the format:
 */
struct sctp_remote_error {
	__u16 sre_type;
	__u16 sre_flags;
	__u32 sre_length;
	__u16 sre_error;
	sctp_assoc_t sre_assoc_id;
	__u8 sre_data[0];
};


/*
 * 6.1.4. SCTP_SEND_FAILED
 *
 *   If SCTP cannot deliver a message it may return the message as a
 *   notification.
 *
 *   (DEPRECATED) - Please note that this notification is deprecated.  Use
 *                  SCTP_SEND_FAILED_EVENT instead.
 *
 */
struct sctp_send_failed {
	__u16 ssf_type;
	__u16 ssf_flags;
	__u32 ssf_length;
	__u32 ssf_error;
	struct sctp_sndrcvinfo ssf_info;
	sctp_assoc_t ssf_assoc_id;
	__u8 ssf_data[0];
};

/*
 *   ssf_flags: 16 bits (unsigned integer)
 *
 *   The flag value will take one of the following values
 *
 *   SCTP_DATA_UNSENT  - Indicates that the data was never put on
 *                       the wire.
 *
 *   SCTP_DATA_SENT    - Indicates that the data was put on the wire.
 *                       Note that this does not necessarily mean that the
 *                       data was (or was not) successfully delivered.
 */
enum sctp_ssf_flags {
	SCTP_DATA_UNSENT,
	SCTP_DATA_SENT,
};

/*
 * 6.1.5. SCTP_SHUTDOWN_EVENT
 *
 *   When a peer sends a SHUTDOWN, SCTP delivers this notification to
 *   inform the application that it should cease sending data.
 */
struct sctp_shutdown_event {
	__u16 sse_type;
	__u16 sse_flags;
	__u32 sse_length;
	sctp_assoc_t sse_assoc_id;
};

/*
 * 6.1.6 SCTP_ADAPTATION_INDICATION
 *
 *   When a peer sends a Adaptation Layer Indication parameter , SCTP
 *   delivers this notification to inform the application
 *   that of the peers requested adaptation layer.
 */
struct sctp_adaptation_event {
	__u16 sai_type;
	__u16 sai_flags;
	__u32 sai_length;
	__u32 sai_adaptation_ind;
	sctp_assoc_t sai_assoc_id;
};

/*
 * 6.1.7 SCTP_PARTIAL_DELIVERY_EVENT
 *
 *   When a receiver is engaged in a partial delivery of a
 *   message this notification will be used to indicate
 *   various events.
 */
struct sctp_pdapi_event {
	__u16 pdapi_type;
	__u16 pdapi_flags;
	__u32 pdapi_length;
	__u32 pdapi_indication;
	sctp_assoc_t pdapi_assoc_id;
};

enum { SCTP_PARTIAL_DELIVERY_ABORTED=0, };

/*
 * 6.1.8.  SCTP_AUTHENTICATION_EVENT
 *
 *  When a receiver is using authentication this message will provide
 *  notifications regarding new keys being made active as well as errors.
 */
struct sctp_authkey_event {
	__u16 auth_type;
	__u16 auth_flags;
	__u32 auth_length;
	__u16 auth_keynumber;
	__u16 auth_altkeynumber;
	__u32 auth_indication;
	sctp_assoc_t auth_assoc_id;
};

enum { SCTP_AUTH_NEWKEY = 0, };

/*
 * 6.1.9. SCTP_SENDER_DRY_EVENT (DEPRECATED)
 *
 * When the SCTP stack has no more user data to send or retransmit, this
 * notification is given to the user. Also, at the time when a user app
 * subscribes to this event, if there is no data to be sent or
 * retransmit, the stack will immediately send up this notification.
 */
struct sctp_sender_dry_event {
	__u16 sender_dry_type;
	__u16 sender_dry_flags;
	__u32 sender_dry_length;
	sctp_assoc_t sender_dry_assoc_id;
};

/*
 * 6.1.11. SCTP_SEND_FAILED_EVENT
 *
 *   If SCTP cannot deliver a message, it can return back the message as a
 *   notification if the SCTP_SEND_FAILED_EVENT event is enabled
 */

struct sctp_send_failed_event {
	__u16 ssfe_type;
	__u16 ssfe_flags;
	__u32 ssfe_length;
	__u32 ssfe_error;
	struct sctp_sndinfo ssfe_info;
	sctp_assoc_t ssfe_assoc_id;
	__u8  ssfe_data[];
};

/*
 *
 * 6.2.2. SCTP_EVENT Option
 *
 *   The SCTP_EVENTS socket option has one issue for future compatibility.
 *   As new features are added, the structure (sctp_event_subscribe) must
 *   be expanded.  This can cause an application binary interface (ABI)
 *   issue unless an implementation has added padding at the end of the
 *   structure.  To avoid this problem, SCTP_EVENTS has been deprecated
 *   and a new socket option SCTP_EVENT has taken its place.
 */

struct sctp_event {
	sctp_assoc_t se_assoc_id;
	__u16 se_type;
	__u8 se_on;
};

/*
 * Described in Section 6.2.1.
 *   Ancillary Data and Notification Interest Options
 *
 *   (DEPRECATED) - Use the SCTP_EVENT option described in Section 6.2.2.
 *                  instead.
 */
struct sctp_event_subscribe {
	__u8 sctp_data_io_event;
	__u8 sctp_association_event;
	__u8 sctp_address_event;
	__u8 sctp_send_failure_event;
	__u8 sctp_peer_error_event;
	__u8 sctp_shutdown_event;
	__u8 sctp_partial_delivery_event;
	__u8 sctp_adaptation_layer_event;
	__u8 sctp_authentication_event;
	__u8 sctp_sender_dry_event;
};

/*
 * 6.1 SCTP Notification Structure
 *
 *   The notification structure is defined as the union of all
 *   notification types.
 *
 */
union sctp_notification {
	struct sctp_tlv {
		__u16 sn_type;             /* Notification type. */
		__u16 sn_flags;
		__u32 sn_length;
	} sn_header;
	struct sctp_assoc_change sn_assoc_change;
	struct sctp_paddr_change sn_paddr_change;
	struct sctp_remote_error sn_remote_error;
	struct sctp_send_failed sn_send_failed;
	struct sctp_shutdown_event sn_shutdown_event;
	struct sctp_adaptation_event sn_adaptation_event;
	struct sctp_pdapi_event sn_pdapi_event;
	struct sctp_authkey_event sn_authkey_event;
	struct sctp_sender_dry_event sn_sender_dry_event;
	struct sctp_send_failed_event sn_send_failed_event;
};

/* Section 5.3.1
 * All standard values for sn_type flags are greater than 2^15.
 * Values from 2^15 and down are reserved.
 */

enum sctp_sn_type {
	SCTP_SN_TYPE_BASE     = (1<<15),
	SCTP_ASSOC_CHANGE,
#define SCTP_ASSOC_CHANGE		SCTP_ASSOC_CHANGE
	SCTP_PEER_ADDR_CHANGE,
#define SCTP_PEER_ADDR_CHANGE		SCTP_PEER_ADDR_CHANGE
	SCTP_SEND_FAILED,
#define SCTP_SEND_FAILED		SCTP_SEND_FAILED
	SCTP_REMOTE_ERROR,
#define SCTP_REMOTE_ERROR		SCTP_REMOTE_ERROR
	SCTP_SHUTDOWN_EVENT,
#define SCTP_SHUTDOWN_EVENT		SCTP_SHUTDOWN_EVENT
	SCTP_PARTIAL_DELIVERY_EVENT,
#define SCTP_PARTIAL_DELIVERY_EVENT	SCTP_PARTIAL_DELIVERY_EVENT
	SCTP_ADAPTATION_INDICATION,
#define SCTP_ADAPTATION_INDICATION	SCTP_ADAPTATION_INDICATION
	SCTP_AUTHENTICATION_EVENT,
#define SCTP_AUTHENTICATION_INDICATION	SCTP_AUTHENTICATION_EVENT
	SCTP_SENDER_DRY_EVENT,
#define SCTP_SENDER_DRY_EVENT		SCTP_SENDER_DRY_EVENT
};

/* Notification error codes used to fill up the error fields in some
 * notifications.
 * SCTP_PEER_ADDRESS_CHAGE 	: spc_error
 * SCTP_ASSOC_CHANGE		: sac_error
 * These names should be potentially included in the draft 04 of the SCTP
 * sockets API specification.
 */
typedef enum sctp_sn_error {
	SCTP_FAILED_THRESHOLD,
	SCTP_RECEIVED_SACK,
	SCTP_HEARTBEAT_SUCCESS,
	SCTP_RESPONSE_TO_USER_REQ,
	SCTP_INTERNAL_ERROR,
	SCTP_SHUTDOWN_GUARD_EXPIRES,
	SCTP_PEER_FAULTY,
} sctp_sn_error_t;

/*
 * 8.1.1 Retransmission Timeout Parameters (SCTP_RTOINFO)
 *
 *   The protocol parameters used to initialize and bound retransmission
 *   timeout (RTO) are tunable.  See [SCTP] for more information on how
 *   these parameters are used in RTO calculation.
 */
struct sctp_rtoinfo {
	sctp_assoc_t	srto_assoc_id;
	__u32		srto_initial;
	__u32		srto_max;
	__u32		srto_min;
};

/*
 * 8.1.2 Association Parameters (SCTP_ASSOCINFO)
 *
 *   This option is used to both examine and set various association and
 *   endpoint parameters.
 */
struct sctp_assocparams {
	sctp_assoc_t	sasoc_assoc_id;
	__u16		sasoc_asocmaxrxt;
	__u16		sasoc_number_peer_destinations;
	__u32		sasoc_peer_rwnd;
	__u32		sasoc_local_rwnd;
	__u32		sasoc_cookie_life;
};

/*
 * 8.3.1 Set Peer Primary Address (SCTP_SET_PEER_PRIMARY_ADDR)
 *
 *  Requests that the peer mark the enclosed address as the association
 *  primary. The enclosed address must be one of the association's
 *  locally bound addresses. The following structure is used to make a
 *   set primary request:
 */
struct sctp_setpeerprim {
	sctp_assoc_t            sspp_assoc_id;
	struct sockaddr_storage sspp_addr;
} __attribute__((packed, aligned(4)));

/* For backward compatibility use, define the old name too */
#define sctp_setprim sctp_prim

/*
 * 8.1.9 Set Primary Address (SCTP_PRIMARY_ADDR)
 *
 *  Requests that the local SCTP stack use the enclosed peer address as
 *  the association primary. The enclosed address must be one of the
 *  association peer's addresses. The following structure is used to
 *  make a set peer primary request:
 */
struct sctp_setprim {
	sctp_assoc_t            ssp_assoc_id;
	struct sockaddr_storage ssp_addr;
} __attribute__((packed, aligned(4)));

/*
 * 8.1.10 Set Adaptation Layer Indicator (SCTP_ADAPTATION_LAYER)
 *
 * Requests that the local endpoint set the specified Adaptation Layer
 * Indication parameter for all future INIT and INIT-ACK exchanges.
 */
struct sctp_setadaptation {
	__u32	ssb_adaptation_ind;
};

/*
 * 8.1.12 Peer Address Parameters  (SCTP_PEER_ADDR_PARAMS)
 *
 *   Applications can enable or disable heartbeats for any peer address
 *   of an association, modify an address's heartbeat interval, force a
 *   heartbeat to be sent immediately, and adjust the address's maximum
 *   number of retransmissions sent before an address is considered
 *   unreachable. The following structure is used to access and modify an
 *   address's parameters:
 */
enum  sctp_spp_flags {
	SPP_HB_ENABLE = 1<<0,		/*Enable heartbeats*/
	SPP_HB_DISABLE = 1<<1,		/*Disable heartbeats*/
	SPP_HB = SPP_HB_ENABLE | SPP_HB_DISABLE,
	SPP_HB_DEMAND = 1<<2,		/*Send heartbeat immediately*/
	SPP_PMTUD_ENABLE = 1<<3,	/*Enable PMTU discovery*/
	SPP_PMTUD_DISABLE = 1<<4,	/*Disable PMTU discovery*/
	SPP_PMTUD = SPP_PMTUD_ENABLE | SPP_PMTUD_DISABLE,
	SPP_SACKDELAY_ENABLE = 1<<5,	/*Enable SACK*/
	SPP_SACKDELAY_DISABLE = 1<<6,	/*Disable SACK*/
	SPP_SACKDELAY = SPP_SACKDELAY_ENABLE | SPP_SACKDELAY_DISABLE,
	SPP_HB_TIME_IS_ZERO = 1<<7,	/* Set HB delay to 0 */
};

struct sctp_paddrparams {
	sctp_assoc_t		spp_assoc_id;
	struct sockaddr_storage	spp_address;
	__u32			spp_hbinterval;
	__u16			spp_pathmaxrxt;
	__u32			spp_pathmtu;
	__u32			spp_sackdelay;
	__u32			spp_flags;
	__u32			spp_ipv6_flowlabel;
	__u8			spp_dscp;
} __attribute__((packed, aligned(4)));

/*
 * 8.3.2.  Add a chunk that must be authenticated (SCTP_AUTH_CHUNK)
 *
 * This set option adds a chunk type that the user is requesting to be
 * received only in an authenticated way.  Changes to the list of chunks
 * will only effect future associations on the socket.
 */
struct sctp_authchunk {
	__u8		sauth_chunk;
};

/*
 * 8.1.17.  Get or set the list of supported HMAC Identifiers (SCTP_HMAC_IDENT)
 *
 * This option gets or sets the list of HMAC algorithms that the local
 * endpoint requires the peer to use.
 */
#ifndef __KERNEL__
/* This here is only used by user space as is. It might not be a good idea
 * to export/reveal the whole structure with reserved fields etc.
 */
enum {
	SCTP_AUTH_HMAC_ID_SHA1 = 1,
	SCTP_AUTH_HMAC_ID_SHA256 = 3,
};
#endif

struct sctp_hmacalgo {
	__u32		shmac_num_idents;
	__u16		shmac_idents[];
};

/* Sadly, user and kernel space have different names for
 * this structure member, so this is to not break anything.
 */
#define shmac_number_of_idents	shmac_num_idents

/*
 * 8.3.3.  Set a shared key (SCTP_AUTH_KEY)
 *
 * This option will set a shared secret key which is used to build an
 * association shared key.
 */
struct sctp_authkey {
	sctp_assoc_t	sca_assoc_id;
	__u16		sca_keynumber;
	__u16		sca_keylength;
	__u8		sca_key[];
};

/*
 * 8.1.18.  Get or set the active shared key (SCTP_AUTH_ACTIVE_KEY)
 *
 * This option will get or set the active shared key to be used to build
 * the association shared key.
 */

struct sctp_authkeyid {
	sctp_assoc_t	scact_assoc_id;
	__u16		scact_keynumber;
};


/*
 * 8.1.19.  Get or set delayed ack timer (SCTP_DELAYED_SACK)
 *
 * This option will effect the way delayed acks are performed.  This
 * option allows you to get or set the delayed ack time, in
 * milliseconds.  It also allows changing the delayed ack frequency.
 * Changing the frequency to 1 disables the delayed sack algorithm.  If
 * the assoc_id is 0, then this sets or gets the endpoints default
 * values.  If the assoc_id field is non-zero, then the set or get
 * effects the specified association for the one to many model (the
 * assoc_id field is ignored by the one to one model).  Note that if
 * sack_delay or sack_freq are 0 when setting this option, then the
 * current values will remain unchanged.
 */
struct sctp_sack_info {
	sctp_assoc_t	sack_assoc_id;
	uint32_t	sack_delay;
	uint32_t	sack_freq;
};

struct sctp_assoc_value {
    sctp_assoc_t            assoc_id;
    uint32_t                assoc_value;
};

/*
 * 8.2.2 Peer Address Information (SCTP_GET_PEER_ADDR_INFO)
 *
 *   Applications can retrieve information about a specific peer address
 *   of an association, including its reachability state, congestion
 *   window, and retransmission timer values.  This information is
 *   read-only. The following structure is used to access this
 *   information:
 */
struct sctp_paddrinfo {
	sctp_assoc_t		spinfo_assoc_id;
	struct sockaddr_storage	spinfo_address;
	__s32			spinfo_state;
	__u32			spinfo_cwnd;
	__u32			spinfo_srtt;
	__u32			spinfo_rto;
	__u32			spinfo_mtu;
} __attribute__((packed, aligned(4)));

/* Peer addresses's state. */
/* UNKNOWN: Peer address passed by the upper layer in sendmsg or connect[x]
 * calls.
 * UNCONFIRMED: Peer address received in INIT/INIT-ACK address parameters.
 *              Not yet confirmed by a heartbeat and not available for data
 *		transfers.
 * ACTIVE : Peer address confirmed, active and available for data transfers.
 * INACTIVE: Peer address inactive and not available for data transfers.
 */
enum sctp_spinfo_state {
	SCTP_INACTIVE,
	SCTP_PF,
	SCTP_ACTIVE,
	SCTP_UNCONFIRMED,
	SCTP_UNKNOWN = 0xffff  /* Value used for transport state unknown */
};

/*
 * 8.2.1 Association Status (SCTP_STATUS)
 *
 *   Applications can retrieve current status information about an
 *   association, including association state, peer receiver window size,
 *   number of unacked data chunks, and number of data chunks pending
 *   receipt.  This information is read-only.  The following structure is
 *   used to access this information:
 */
struct sctp_status {
	sctp_assoc_t		sstat_assoc_id;
	__s32			sstat_state;
	__u32			sstat_rwnd;
	__u16			sstat_unackdata;
	__u16			sstat_penddata;
	__u16			sstat_instrms;
	__u16			sstat_outstrms;
	__u32			sstat_fragmentation_point;
	struct sctp_paddrinfo	sstat_primary;
};

/*
 * 8.2.3.  Get the list of chunks the peer requires to be authenticated
 *         (SCTP_PEER_AUTH_CHUNKS)
 *
 * This option gets a list of chunks for a specified association that
 * the peer requires to be received authenticated only.
 */
struct sctp_authchunks {
	sctp_assoc_t	gauth_assoc_id;
	__u32		gauth_number_of_chunks;
	uint8_t		gauth_chunks[];
};

/* The broken spelling has been released already in lksctp-tools header,
 * so don't break anyone, now that it's fixed.
 */
#define guth_number_of_chunks	gauth_number_of_chunks

/* Association states.  */
enum sctp_sstat_state {
	SCTP_EMPTY                = 0,
	SCTP_CLOSED               = 1,
	SCTP_COOKIE_WAIT          = 2,
	SCTP_COOKIE_ECHOED        = 3,
	SCTP_ESTABLISHED          = 4,
	SCTP_SHUTDOWN_PENDING     = 5,
	SCTP_SHUTDOWN_SENT        = 6,
	SCTP_SHUTDOWN_RECEIVED    = 7,
	SCTP_SHUTDOWN_ACK_SENT    = 8,
};

/*
 * 8.2.6. Get the Current Identifiers of Associations
 *        (SCTP_GET_ASSOC_ID_LIST)
 *
 * This option gets the current list of SCTP association identifiers of
 * the SCTP associations handled by a one-to-many style socket.
 */
struct sctp_assoc_ids {
	__u32		gaids_number_of_ids;
	sctp_assoc_t	gaids_assoc_id[];
};

/*
 * 8.3, 8.5 get all peer/local addresses in an association.
 * This parameter struct is used by SCTP_GET_PEER_ADDRS and
 * SCTP_GET_LOCAL_ADDRS socket options used internally to implement
 * sctp_getpaddrs() and sctp_getladdrs() API.
 */
struct sctp_getaddrs_old {
	sctp_assoc_t            assoc_id;
	int			addr_num;
#ifdef __KERNEL__
	struct sockaddr		__user *addrs;
#else
	struct sockaddr		*addrs;
#endif
};

struct sctp_getaddrs {
	sctp_assoc_t		assoc_id; /*input*/
	__u32			addr_num; /*output*/
	__u8			addrs[0]; /*output, variable size*/
};

/* A socket user request obtained via SCTP_GET_ASSOC_STATS that retrieves
 * association stats. All stats are counts except sas_maxrto and
 * sas_obs_rto_ipaddr. maxrto is the max observed rto + transport since
 * the last call. Will return 0 when RTO was not update since last call
 */
struct sctp_assoc_stats {
	sctp_assoc_t	sas_assoc_id;    /* Input */
					 /* Transport of observed max RTO */
	struct sockaddr_storage sas_obs_rto_ipaddr;
	__u64		sas_maxrto;      /* Maximum Observed RTO for period */
	__u64		sas_isacks;	 /* SACKs received */
	__u64		sas_osacks;	 /* SACKs sent */
	__u64		sas_opackets;	 /* Packets sent */
	__u64		sas_ipackets;	 /* Packets received */
	__u64		sas_rtxchunks;   /* Retransmitted Chunks */
	__u64		sas_outofseqtsns;/* TSN received > next expected */
	__u64		sas_idupchunks;  /* Dups received (ordered+unordered) */
	__u64		sas_gapcnt;      /* Gap Acknowledgements Received */
	__u64		sas_ouodchunks;  /* Unordered data chunks sent */
	__u64		sas_iuodchunks;  /* Unordered data chunks received */
	__u64		sas_oodchunks;	 /* Ordered data chunks sent */
	__u64		sas_iodchunks;	 /* Ordered data chunks received */
	__u64		sas_octrlchunks; /* Control chunks sent */
	__u64		sas_ictrlchunks; /* Control chunks received */
};

/* These are bit fields for msghdr->msg_flags.  See section 5.1.  */
/* On user space Linux, these live in <bits/socket.h> as an enum.  */
enum sctp_msg_flags {
	MSG_NOTIFICATION = 0x8000,
#define MSG_NOTIFICATION MSG_NOTIFICATION
};

/*
 * 8.1 sctp_bindx()
 *
 * The flags parameter is formed from the bitwise OR of zero or more of the
 * following currently defined flags:
 */
#define SCTP_BINDX_ADD_ADDR 0x01
#define SCTP_BINDX_REM_ADDR 0x02

/* This is the structure that is passed as an argument(optval) to
 * getsockopt(SCTP_SOCKOPT_PEELOFF).
 */
typedef struct {
	sctp_assoc_t associd;
	int sd;
} sctp_peeloff_arg_t;

/*
 *  Peer Address Thresholds socket option
 */
struct sctp_paddrthlds {
	sctp_assoc_t spt_assoc_id;
	struct sockaddr_storage spt_address;
	__u16 spt_pathmaxrxt;
	__u16 spt_pathpfthld;
};


/* 8.1.32. Set Default PR-SCTP Parameters (SCTP_DEFAULT_PRINFO)
 *
 * This option sets and gets the default parameters for PR-SCTP.
 *
 */
struct sctp_default_prinfo {
	__u16 pr_policy;
	__u32 pr_value;
	sctp_assoc_t pr_assoc_id;
};

/* These are the sendv_flags in the sctp_sendv_spa struct */
#define SCTP_SEND_SNDINFO_VALID		0x00000001
#define SCTP_SEND_PRINFO_VALID		0x00000002
#define SCTP_SEND_AUTHINFO_VALID	0x00000004

struct sctp_sendv_spa {
    __u32 sendv_flags;
    struct sctp_sndinfo sendv_sndinfo;
    struct sctp_prinfo sendv_prinfo;
    struct sctp_authinfo sendv_authinfo;
};

#define SCTP_SENDV_NOINFO	0
#define SCTP_SENDV_SNDINFO	1
#define SCTP_SENDV_PRINFO	2
#define SCTP_SENDV_AUTHINFO	3
#define SCTP_SENDV_SPA		4

struct sctp_recvv_rn {
    struct sctp_rcvinfo recvv_rcvinfo;
    struct sctp_nxtinfo recvv_nxtinfo;
};

#define SCTP_RECVV_NOINFO	0
#define SCTP_RECVV_RCVINFO	1
#define SCTP_RECVV_NXTINFO	2
#define SCTP_RECVV_RN		3


/*
 * 8.1 sctp_bindx()
 *
 * The flags parameter is formed from the bitwise OR of zero or more of the
 * following currently defined flags:
 */
#define SCTP_BINDX_ADD_ADDR 0x01
#define SCTP_BINDX_REM_ADDR 0x02


int sctp_bindx(int sd, struct sockaddr *addrs, int addrcnt, int flags);

int sctp_connectx(int sd, struct sockaddr *addrs, int addrcnt,
		  sctp_assoc_t *id);

int sctp_peeloff(int sd, sctp_assoc_t assoc_id);

/* Prototype for the library function sctp_opt_info defined in
 * API 7. Socket Options.
 */
int sctp_opt_info(int sd, sctp_assoc_t id, int opt, void *arg, socklen_t *size);

/* Get all peer address on a socket.  This is a new SCTP API
 * described in the section 8.3 of the Sockets API Extensions for SCTP.
 * This is implemented using the getsockopt() interface.
 */
int sctp_getpaddrs(int sd, sctp_assoc_t id, struct sockaddr **addrs);

/* Frees all resources allocated by sctp_getpaddrs().  This is a new SCTP API
 * described in the section 8.4 of the Sockets API Extensions for SCTP.
 */
int sctp_freepaddrs(struct sockaddr *addrs);

/* Get all locally bound address on a socket.  This is a new SCTP API
 * described in the section 8.5 of the Sockets API Extensions for SCTP.
 * This is implemented using the getsockopt() interface.
 */
int sctp_getladdrs(int sd, sctp_assoc_t id, struct sockaddr **addrs);

/* Frees all resources allocated by sctp_getladdrs().  This is a new SCTP API
 * described in the section 8.6 of the Sockets API Extensions for SCTP.
 */
int sctp_freeladdrs(struct sockaddr *addrs);

/* This library function assists the user with the advanced features
 * of SCTP.  This is a new SCTP API described in the section 8.7 of the
 * Sockets API Extensions for SCTP. This is implemented using the
 * sendmsg() interface.
 */
int sctp_sendmsg(int s, const void *msg, size_t len, struct sockaddr *to,
		 socklen_t tolen, uint32_t ppid, uint32_t flags,
		 uint16_t stream_no, uint32_t timetolive, uint32_t context);

/* This library function assist the user with sending a message without
 * dealing directly with the CMSG header.
 */
int sctp_send(int s, const void *msg, size_t len,
              const struct sctp_sndrcvinfo *sinfo, int flags);

/* RFC6458 - Section 9.12*/
int sctp_sendv(int sd,
        const struct iovec *iov,
	int iovlen,
        struct sockaddr *addrs,
        int addrcnt,
        void *info,
        socklen_t infolen,
        unsigned int infotype,
        int flags);

/* RFC6458 - Section 9.13*/
int sctp_recvv(int sd,
        const struct iovec *iov,
        int iovlen,
        struct sockaddr *from,
        socklen_t *fromlen,
        void *info,
        socklen_t *infolen,
        unsigned int *infotype,
        int *flags);

/* This library function assists the user with the advanced features
 * of SCTP.  This is a new SCTP API described in the section 8.8 of the
 * Sockets API Extensions for SCTP. This is implemented using the
 * recvmsg() interface.
 */
int sctp_recvmsg(int s, void *msg, size_t len, struct sockaddr *from,
		 socklen_t *fromlen, struct sctp_sndrcvinfo *sinfo,
		 int *msg_flags);

/* Return the address length for an address family. */
int sctp_getaddrlen(sa_family_t family);

__END_DECLS

#endif /* __linux_sctp_h__ */
