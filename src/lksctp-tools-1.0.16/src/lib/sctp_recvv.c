#include <string.h>
#include <sys/socket.h>   /* struct sockaddr_storage, setsockopt() */
#include <netinet/sctp.h>
#include <errno.h>
#include <stdio.h>
#define SCTP_CONTROL_VEC_SIZE_RCV 16384

int sctp_recvv(int sd,
        const struct iovec *iov,
        int iovlen,
        struct sockaddr *from,
        socklen_t *fromlen,
        void *info,
        socklen_t *infolen,
        unsigned int *infotype,
        int *flags){
        char cmsgbuf[SCTP_CONTROL_VEC_SIZE_RCV];
        struct msghdr msg;
        struct cmsghdr *cmsg;
        ssize_t ret;
        struct sctp_rcvinfo *rcvinfo;
        struct sctp_nxtinfo *nxtinfo;

        if (((info != NULL) && (infolen == NULL)) |
            ((info == NULL) && (infolen != NULL) && (*infolen != 0)) ||
            ((info != NULL) && (infotype == NULL))) {
                errno = EINVAL;
                return (-1);
        }
        if (infotype) {
                *infotype = SCTP_RECVV_NOINFO;
        }
        msg.msg_name = from;
        if (fromlen == NULL) {
                msg.msg_namelen = 0;
        } else {
                msg.msg_namelen = *fromlen;
        }
        msg.msg_iov = (struct iovec *)iov;
        msg.msg_iovlen = iovlen;
        msg.msg_control = cmsgbuf;
        msg.msg_controllen = sizeof(cmsgbuf);
        ret = recvmsg(sd, &msg, *flags);
        *flags = msg.msg_flags;
        /*printf("%s: GOVX: Flags = %x (%d)\n",__func__, *flags, *flags);*/
        /*printf("%s: GOVX: melding: \"%s\"\n",__func__, msg.msg_iov->iov_base);*/
        printf("libsctp: Har fått adresse til info: %d\n", info);
        if ((ret > 0) &&
            (msg.msg_controllen > 0) &&
            (infotype != NULL) &&
            (infolen != NULL) &&
            (*infolen > 0)) {
                rcvinfo = NULL;
                nxtinfo = NULL;
                for (cmsg = CMSG_FIRSTHDR(&msg); cmsg; cmsg = CMSG_NXTHDR(&msg, cmsg)) {
                        if (cmsg->cmsg_level != IPPROTO_SCTP) {
                                /*printf("%s: GOVX: Fant noe annet enn en IPPROTO_SCTP header\n", __func__);*/
                                continue;
                        }
                        if (cmsg->cmsg_type == SCTP_RCVINFO) {
                                printf("%s: libsctp GOVX: Fant en SCTP_RCVINFO header her!\n", __func__);
                                rcvinfo = (struct sctp_rcvinfo *)CMSG_DATA(cmsg);
                                /*printf("%s: GOVX: rcvinfo->rcv_ppid = %d\n", __func__, rcvinfo->rcv_ppid);*/
                                if (nxtinfo != NULL) {
                                        break;
                                } else {
                                        continue;
                                }
                        }
                        if (cmsg->cmsg_type == SCTP_NXTINFO) {
                                /*printf("%s: GOVX: Fant en SCTP_NXTINFO header her!\n", __func__);*/
                                /*printf("%s: GOVX: SCTP_NXTINFO(%d) == cmsg->cmsg_type(%d) og SCTP_RCVINFO = %d\n", __func__ ,SCTP_NXTINFO, cmsg->cmsg_type, SCTP_RCVINFO);*/
                                nxtinfo = (struct sctp_nxtinfo *)CMSG_DATA(cmsg);
                                if (rcvinfo != NULL) {
                                        break;
                                } else {
                                        continue;
                                }
                        }
                }
                if (rcvinfo != NULL) {
                        if ((nxtinfo != NULL) && (*infolen >= sizeof(struct sctp_recvv_rn))) {
                            /*printf("%s: GOVX: Vi trenger en sctp_recvv_rn\n", __func__);*/
                                struct sctp_recvv_rn *rn_info;
                                
                                rn_info = (struct sctp_recvv_rn *)info;
                                rn_info->recvv_rcvinfo = *rcvinfo;
                                rn_info->recvv_nxtinfo = *nxtinfo;
                                *infolen = (socklen_t) sizeof(struct sctp_recvv_rn);
                                *infotype = SCTP_RECVV_RN;
                        } else if (*infolen >= sizeof(struct sctp_rcvinfo)) {
                                memcpy(info, rcvinfo, sizeof(struct sctp_rcvinfo));
                                *infolen = (socklen_t) sizeof(struct sctp_rcvinfo);
                                *infotype = SCTP_RECVV_RCVINFO;
                        }
                } else if (nxtinfo != NULL) {                        
                        if (*infolen >= sizeof(struct sctp_nxtinfo)) {
                                memcpy(info, nxtinfo, sizeof(struct sctp_nxtinfo));
                                *infolen = (socklen_t) sizeof(struct sctp_nxtinfo);
                                *infotype = SCTP_RECVV_NXTINFO;
                        }
                }
        }
        return (ret);

}
