#include <string.h>
#include <stdlib.h>
#include <sys/socket.h>   /* struct sockaddr_storage, setsockopt() */
#include <netinet/in.h>
#include <netinet/sctp.h>
#include <errno.h>

int sctp_sendv(int sd, const struct iovec *iov, int iovcnt,
        struct sockaddr *addrs, int addrcnt, void *info,
        socklen_t infolen, sctp_cmsg_t infotype, int flags){
/*
 * Use SCTP_DSTADDRV4/SCTP_DSTADDRV6 to implement sctp_sendv using sendmsg().
 * sendmsg signature:
 * sendmsg(int sockfd, const struct msghdr *msg, int flags);
 *
 * Check info, can be one of the following types:
 *
 * SCTP_SENDV_NOINFO
 * SCTP_SENDV_SNDINFO
 * SCTP_SENDV_PRINFO
 * SCTP_SENDV_AUTHINFO
 * SCTP_SENDV_SPA
 */
	struct msghdr msg;
	struct cmsghdr *cmsg;
	struct sctp_sendv_spa *spa_info;
	int i;
	socklen_t addr_len;
	struct sockaddr *addr;
	struct sockaddr_in *addr_in;
	struct sockaddr_in6 *addr_in6;
	in_port_t port;
	int ret;

	if ((addrcnt < 0) ||
	    (iovcnt < 0) ||
	    ((addrs == NULL) && (addrcnt > 0)) ||
	    ((addrs != NULL) && (addrcnt == 0)) ||
	    ((iov == NULL) && (iovcnt > 0)) ||
	    ((iov != NULL) && (iovcnt == 0))) {
		errno = EINVAL;
		return (-1);
	}

	char *outcmsg = malloc(CMSG_SPACE(sizeof(struct sctp_sndinfo)) +
		CMSG_SPACE(sizeof(struct sctp_prinfo)) +
		CMSG_SPACE(sizeof(struct sctp_authinfo)) +
		(addrcnt * CMSG_SPACE(sizeof(struct in6_addr))));
	if (outcmsg == NULL) {
		errno = ENOMEM;
		return (-1);
	}
	msg.msg_control = outcmsg;
	msg.msg_controllen = 0;

	cmsg = (struct cmsghdr *)outcmsg;
	printf("%s: GOVX melding i lksctp: %s\n", __func__, iov->iov_base); 


	switch(infotype) {
	case SCTP_SENDV_NOINFO:
		if((infolen != 0) || (info != NULL)) {
			free(outcmsg);
			errno = EINVAL;
			return (-1);
		}
		break;
        case SCTP_SENDV_SNDINFO:
                if ((info == NULL) || (infolen < sizeof(struct sctp_sndinfo))) {
                        free(outcmsg);
                        errno = EINVAL;
                        return (-1);
                }
                cmsg->cmsg_level = IPPROTO_SCTP;
                cmsg->cmsg_type = SCTP_SNDINFO;
                cmsg->cmsg_len = CMSG_LEN(sizeof(struct sctp_sndinfo));
                memcpy(CMSG_DATA(cmsg), info, sizeof(struct sctp_sndinfo));
                msg.msg_controllen += CMSG_SPACE(sizeof(struct sctp_sndinfo));
                cmsg = (struct cmsghdr *)((caddr_t)cmsg + CMSG_SPACE(sizeof(struct sctp_sndinfo)));
                break;
        case SCTP_SENDV_PRINFO:
                if ((info == NULL) || (infolen < sizeof(struct sctp_prinfo))) {
                        free(outcmsg);
                        errno = EINVAL;
                        return (-1);
                }
                cmsg->cmsg_level = IPPROTO_SCTP;
                cmsg->cmsg_type = SCTP_PRINFO;
                cmsg->cmsg_len = CMSG_LEN(sizeof(struct sctp_prinfo));
                memcpy(CMSG_DATA(cmsg), info, sizeof(struct sctp_prinfo));
                msg.msg_controllen += CMSG_SPACE(sizeof(struct sctp_prinfo));
                cmsg = (struct cmsghdr *)((caddr_t)cmsg + CMSG_SPACE(sizeof(struct sctp_prinfo)));
                break;
        case SCTP_SENDV_AUTHINFO:
                if ((info == NULL) ||
		    (infolen < sizeof(struct sctp_authinfo))) {
                        free(outcmsg);
                        errno = EINVAL;
                        return (-1);
		}
                cmsg->cmsg_level = IPPROTO_SCTP;
                cmsg->cmsg_type = SCTP_AUTHINFO;
                cmsg->cmsg_len = CMSG_LEN(sizeof(struct sctp_authinfo));
                memcpy(CMSG_DATA(cmsg), info, sizeof(struct sctp_authinfo));
                msg.msg_controllen += CMSG_SPACE(sizeof(struct sctp_authinfo));
                cmsg = (struct cmsghdr *)((caddr_t)cmsg + CMSG_SPACE(sizeof(struct sctp_authinfo)));
                break;
        case SCTP_SENDV_SPA:
                if ((info == NULL) ||
		    (infolen < sizeof(struct sctp_sendv_spa))) {
                        free(outcmsg);
                        errno = EINVAL;
                        return (-1);
                }
                spa_info = (struct sctp_sendv_spa *)info;
                if (spa_info->sendv_flags & SCTP_SEND_SNDINFO_VALID) {
                        cmsg->cmsg_level = IPPROTO_SCTP;
                        cmsg->cmsg_type = SCTP_SNDINFO;
                        cmsg->cmsg_len = CMSG_LEN(sizeof(struct sctp_sndinfo));
                        memcpy(CMSG_DATA(cmsg), &spa_info->sendv_sndinfo, sizeof(struct sctp_sndinfo));
                        msg.msg_controllen += CMSG_SPACE(sizeof(struct sctp_sndinfo));
                        cmsg = (struct cmsghdr *)((caddr_t)cmsg + CMSG_SPACE(sizeof(struct sctp_sndinfo)));
                }
                if (spa_info->sendv_flags & SCTP_SEND_PRINFO_VALID) {
                        cmsg->cmsg_level = IPPROTO_SCTP;
                        cmsg->cmsg_type = SCTP_PRINFO;
                        cmsg->cmsg_len = CMSG_LEN(sizeof(struct sctp_prinfo));
                        memcpy(CMSG_DATA(cmsg), &spa_info->sendv_prinfo, sizeof(struct sctp_prinfo));
                        msg.msg_controllen += CMSG_SPACE(sizeof(struct sctp_prinfo));
                        cmsg = (struct cmsghdr *)((caddr_t)cmsg + CMSG_SPACE(sizeof(struct sctp_prinfo)));
                }
                if (spa_info->sendv_flags & SCTP_SEND_AUTHINFO_VALID) {
                        cmsg->cmsg_level = IPPROTO_SCTP;
                        cmsg->cmsg_type = SCTP_AUTHINFO;
                        cmsg->cmsg_len = CMSG_LEN(sizeof(struct sctp_authinfo));
                        memcpy(CMSG_DATA(cmsg), &spa_info->sendv_authinfo, sizeof(struct sctp_authinfo));
                        msg.msg_controllen += CMSG_SPACE(sizeof(struct sctp_authinfo));
                        cmsg = (struct cmsghdr *)((caddr_t)cmsg + CMSG_SPACE(sizeof(struct sctp_authinfo)));
                }
                break;
        default:
                free(outcmsg);
                errno = EINVAL;
                return (-1);
        }
        addr = addrs;
	/*printf("libsctp: adresse: %s\n", inet_ntop(((struct sockaddr_in*) addr)->sin_addr.s_addr));*/
        msg.msg_name = NULL;
        msg.msg_namelen = 0;

        for (i = 0; i < addrcnt; i++) {
                switch (addr->sa_family) {
                case AF_INET:
                        addr_len = (socklen_t) sizeof(struct sockaddr_in);
                        addr_in = (struct sockaddr_in *)addr;
			/*
			 * Not certain if this check is necessary anymore (Linux
			 * does not have a sin_len member).
                         *if (addr_in->sin_len != addr_len) {
                         *        free(outcmsg);
                         *        errno = EINVAL;
                         *        return (-1);
                         *}
			 */
                        if (i == 0) {
                                port = addr_in->sin_port;
                        } else {
                                if (port == addr_in->sin_port) {
                                        cmsg->cmsg_level = IPPROTO_SCTP;
                                        cmsg->cmsg_type = SCTP_DSTADDRV4;
                                        cmsg->cmsg_len = CMSG_LEN(sizeof(struct in_addr));
                                        memcpy(CMSG_DATA(cmsg), &addr_in->sin_addr, sizeof(struct in_addr));
                                        msg.msg_controllen += CMSG_SPACE(sizeof(struct in_addr));
                                        cmsg = (struct cmsghdr *)((caddr_t)cmsg + CMSG_SPACE(sizeof(struct in_addr)));
                                } else {
                                        free(outcmsg);
                                        errno = EINVAL;
                                        return (-1);
                                }
                        }
                        break;
                case AF_INET6:
                        addr_len = (socklen_t) sizeof(struct sockaddr_in6);
                        addr_in6 = (struct sockaddr_in6 *)addr;
			/*
                         *if (addr_in6->sin6_len != addr_len) {
                         *        free(outcmsg);
                         *        errno = EINVAL;
                         *        return (-1);
                         *}
			 */
                        if (i == 0) {
                                port = addr_in6->sin6_port;
                        } else {
                                if (port == addr_in6->sin6_port) {
                                        cmsg->cmsg_level = IPPROTO_SCTP;
                                        cmsg->cmsg_type = SCTP_DSTADDRV6;
                                        cmsg->cmsg_len = CMSG_LEN(sizeof(struct in6_addr));
                                        memcpy(CMSG_DATA(cmsg), &addr_in6->sin6_addr, sizeof(struct in6_addr));
                                        msg.msg_controllen += CMSG_SPACE(sizeof(struct in6_addr));
                                        cmsg = (struct cmsghdr *)((caddr_t)cmsg + CMSG_SPACE(sizeof(struct in6_addr)));
                                } else {
                                        free(outcmsg);
                                        errno = EINVAL;
                                        return (-1);
                                }
                        }
                        break;
                default:
                        free(outcmsg);
                        errno = EINVAL;
                        return (-1);
                }
                if (i == 0) {
                        msg.msg_name = addr;
                        msg.msg_namelen = addr_len;
                }
                addr = (struct sockaddr *)((caddr_t)addr + addr_len);
        }
        if (msg.msg_controllen == 0) {
                msg.msg_control = NULL;
        }
        msg.msg_iov = (struct iovec *)iov;
        msg.msg_iovlen = iovcnt;
        msg.msg_flags = 0;
        ret = sendmsg(sd, &msg, flags);
	if(ret == -1){
		printf("libsctp: oops... %s\n", strerror(errno));
	}
        free(outcmsg);
        return (ret);

	}

